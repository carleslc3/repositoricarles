package iam.carleslopez.lliga;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Partit implements Serializable{

	private Equip local;
	private Equip visitant;
	private ArrayList<Gol> golsLocal;
	private ArrayList<Gol> golsVisitant;

	public Partit(Equip equip1, Equip equip2) {
		local = equip1;
		visitant = equip2;
		this.golsLocal = new ArrayList<>();
		this.golsVisitant = new ArrayList<>();
	}

	public void jugaPartit() {

		Random r = new Random();
		int qGols1 = r.nextInt(6), qGols2 = r.nextInt(6),jugadorGol,minutGol;	
		
		for (int i = 0; i < qGols1; i++) {
			jugadorGol = r.nextInt(local.getJugadors().size()-1)+1;
			minutGol = r.nextInt(90);
			
			Gol golLocal = new Gol(local.getJugadors().get(jugadorGol),minutGol);
			golsLocal.add(golLocal);
			System.out.println("Gol Local ("+ local.getNom() +") de " + golLocal.getJugador() + " al minut " + golLocal.getMinutMarcat());
		}
		
		for (int i = 0; i < qGols2; i++) {
			jugadorGol = r.nextInt(visitant.getJugadors().size()-1)+1;
			minutGol = r.nextInt(90);
			
			Gol golVisitant = new Gol(visitant.getJugadors().get(jugadorGol),minutGol);
			golsVisitant.add(golVisitant);
			System.out.println("Gol Visitant ("+ visitant.getNom() +") de " + golVisitant.getJugador() + " al minut " + golVisitant.getMinutMarcat());
		}
		
		System.out.println(fi());
	}

	public String fi() {
		if (golsLocal.size() > golsVisitant.size()) {
			local.incrementaPunts(3);
		} else if (golsLocal.size() < golsVisitant.size()) {
			visitant.incrementaPunts(3);
		} else {
			local.incrementaPunts(1);
			visitant.incrementaPunts(1);
		}
		
		return "\n"+local.getNom() + " " +golsLocal.size() +" - " + golsVisitant.size() +" "+visitant.getNom()+"\n";
	}
}
