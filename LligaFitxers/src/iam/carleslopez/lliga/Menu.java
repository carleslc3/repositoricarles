package iam.carleslopez.lliga;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JOptionPane;

public class Menu {

	Lliga lliga;
	Equip equip;

	public Menu(Lliga lliga, Equip equip) {

		int opcio;
		this.lliga = lliga;
		this.equip = equip;

		do {
			opcio = Integer.parseInt(JOptionPane.showInputDialog(
									"1) Jugar Lliga\n" 
									+ "2) Mostrar Classificacio\n"								
									+ "3) Montar equips per la lliga\n"
									+ "4) Crear un equip\n"
									+ "5) Guardar Lliga\n" 
									+ "6) Sortir\n"
									+ "Tria una opcio(1-4)"));

			if (opcio == 1) {
				lliga.setNom(JOptionPane.showInputDialog("Nom de la lliga"));
				lliga.jugaLliga();
			} else if (opcio == 2) {
				lliga.classificacio();
			} else if (opcio == 3) {
				lliga.crearPartits();
			}else if (opcio == 4){
				equip.creaEquip();
			} else if(opcio == 5){
				guardaLliga();			
			}else {
				JOptionPane.showMessageDialog(null, "Opcio incorrecte!", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		} while (opcio != 6);
	}
	
	private boolean guardaLliga(){
		
		try {
			ObjectOutputStream obOut = new ObjectOutputStream(new FileOutputStream(lliga.getNom()));
			obOut.writeObject(lliga);
			obOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
