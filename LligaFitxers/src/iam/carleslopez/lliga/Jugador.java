package iam.carleslopez.lliga;

import java.io.Serializable;

public class Jugador implements Serializable{

	private String nom, cognom;
	private int dorsal, edat, gols, golsAcumulats;

	public Jugador(String jugador) throws InsuficientsAtributsException {

		String[] infoJugador = jugador.split("#");
		
		if (infoJugador.length < 6){ 
			throw new InsuficientsAtributsException("Falten Atributs al jugador");
		}
		
		try{
			this.dorsal = Integer.parseInt(infoJugador[0]);
			this.cognom = infoJugador[1];
			this.nom = infoJugador[2];
			this.edat = Integer.parseInt(infoJugador[3]);
			this.gols = Integer.parseInt(infoJugador[4]);
			this.golsAcumulats = Integer.parseInt(infoJugador[5]);
		}catch(Exception e){
			throw new InsuficientsAtributsException("Falten Atributs al jugador");
		}
	}

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public int getEdatJugador() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getGols() {
		return gols;
	}

	public void setGols(int gols) {
		this.gols = gols;
	}

	public int getGolsAcumulats() {
		return golsAcumulats;
	}

	public void setGolsAcumulats(int golsAcumulats) {
		this.golsAcumulats = golsAcumulats;
	}

	@Override
	public String toString() {
		return "Jugador [dorsalJugador=" + dorsal + ", nomJugador=" + nom + ", golsAcumulats=" + golsAcumulats + "]";
	}
}
