package iam.carleslopez.lliga;

import java.io.Serializable;

public class Gol implements Serializable{
	private	 Jugador jugador;
	private int minutMarcat;
	
	public Gol(Jugador jugador, int minutMarcat) {
		this.jugador = jugador;
		this.minutMarcat = minutMarcat;
	}

	public String getJugador() {
		return jugador.getNom() + " "+jugador.getCognom();
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public int getMinutMarcat() {
		return minutMarcat;
	}

	public void setMinutMarcat(int minutMarcat) {
		this.minutMarcat = minutMarcat;
	}
}
