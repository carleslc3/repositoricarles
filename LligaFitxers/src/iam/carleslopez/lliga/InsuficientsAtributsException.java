package iam.carleslopez.lliga;

@SuppressWarnings("serial")
public class InsuficientsAtributsException extends Exception {

	public InsuficientsAtributsException(String message) {
		super(message);
	}
}
