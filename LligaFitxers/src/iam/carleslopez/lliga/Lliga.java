package iam.carleslopez.lliga;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Lliga implements Serializable{

	private ArrayList<Equip> equips = new ArrayList<Equip>();
	private ArrayList<Partit> partits = new ArrayList<Partit>();
	private int quantitat, numPartits;
	private File file;
	private String nom;

	public Lliga(Path p) throws IOException {

		this.file = new File(p.getFileName().toString());
		File[] fitxers = file.listFiles();

		for (File fitxerequip : fitxers) {
			Equip e;
			try {
				e = new Equip(fitxerequip);
				equips.add(e);
			} catch (InsuficientsJugadorsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InsuficientsAtributsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		this.quantitat = fitxers.length;
		this.numPartits = quantitat * quantitat - quantitat;
		this.crearPartits();
	}

	public void crearPartits() {
		
		for (int i = 0; i < equips.size(); i++) {
			for (int j = 0; j < equips.size(); j++) {
				if (!equips.get(i).equals(equips.get(j))) {
					Equip equip1 = equips.get(i);
					Equip equip2 = equips.get(j);
					partits.add(new Partit(equip1, equip2));
				}
			}
		}
	}

	public void jugaLliga() {

		for (Partit partit : partits) {
			partit.jugaPartit();
		}
	}

	public void classificacio() {
		for (Equip equip : equips) {
			System.out.println(equip.toString());
		}
	}


	public ArrayList<Equip> getEquipsLliga() {
		return equips;
	}

	public void setEquipsLliga(ArrayList<Equip> equipsLliga) {
		this.equips = equipsLliga;
	}

	public ArrayList<Partit> getPartitsLliga() {
		return partits;
	}

	public void setPartitsLliga(ArrayList<Partit> partitsLliga) {
		this.partits = partitsLliga;
	}

	public int getQuantitatEquips() {
		return quantitat;
	}

	public void setQuantitatEquips(int quantitatEquips) {
		this.quantitat = quantitatEquips;
	}

	public int getNumPartits() {
		return numPartits;
	}

	public void setNumPartits(int numPartits) {
		this.numPartits = numPartits;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
