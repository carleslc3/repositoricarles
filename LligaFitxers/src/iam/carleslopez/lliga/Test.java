package iam.carleslopez.lliga;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Test {

	public static void main(String[] args) throws IOException {
		
		Path ruta = Paths.get("EquipsLliga");	
		Lliga lliga = new Lliga(ruta);
		Equip equip = new Equip();
		
		Menu menu = new Menu(lliga,equip);
	}
}
