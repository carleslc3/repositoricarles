package iam.carleslopez.lliga;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class Equip implements Serializable{

	private String nom;
	private int puntsLliga;
	private Hashtable<Integer, Jugador> equip = new Hashtable<Integer, Jugador>();
	
	public Equip(){
		
	}

	public Equip(String nom) {
		this.nom = nom;
		this.puntsLliga = 0;
	}

	public Equip(File f) throws IOException, InsuficientsJugadorsException, InsuficientsAtributsException {

		this.puntsLliga = 0;
		this.nom = f.getName();
		this.nom = this.nom.substring(0, this.nom.lastIndexOf("."));

		if (f.length() < 15) {
			throw new InsuficientsJugadorsException("Minim hi ha d'haver 15 jugadors a l'equip " + nom);
		}

		BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
		String dades;

		while ((dades = buffer.readLine()) != null) {
			Jugador player;
			try {
				player = new Jugador(dades);
				equip.put(player.getDorsal(), player);
			} catch (InsuficientsAtributsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		buffer.close();
	}

	public void addJugador(String nomEquip, String dades){
		
		try{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("EquipsLliga/"+equip, true)));
			try{
				Jugador player = new Jugador(dades);
				equip.put(player.getDorsal(), player);
		    	out.println(dades);
			}catch(InsuficientsAtributsException e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			out.close();
		}catch (IOException e) { 
			e.printStackTrace();
		}
	}

	public void creaEquip() {
		int quantitatJugadors = 0;
		String input = JOptionPane.showInputDialog("Introdueix nom del Equip", "");
		Path pathToFile = Paths.get("EquipsLliga/" + input);
		try {
			Files.createDirectories(pathToFile.getParent());
			Files.createFile(pathToFile);
			Equip equip = new Equip(input);

			do {
				quantitatJugadors = Integer.parseInt(JOptionPane.showInputDialog(null, "Numero de jugadors a introduir(minim 15):", ""));
				
				if (quantitatJugadors < 15) {
					JOptionPane.showMessageDialog(null, "Hi ha d'haver com a m�nim 15 Jugadors", "ERROR",JOptionPane.ERROR_MESSAGE);
				}
				
			} while (quantitatJugadors < 15);
			
			for (int i = 0; i < quantitatJugadors; i++) {
				String dades = JOptionPane.showInputDialog("Introdueix dades del jugador\nFormat: Dorsal#Cognom#Nom#Edat#GolsAcumulats ");
				equip.addJugador(input, dades);
			}		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Jugador getJugador(int dorsal) {

		return equip.get(dorsal);
	}

	public void incrementaPunts(int punts) {
		this.puntsLliga += punts;
	}

	public String getNom() {
		return nom;
	}

	public int getPuntsLliga() {
		return puntsLliga;
	}

	public void setJugadors(Hashtable<Integer, Jugador> jugadors) {
		this.equip = jugadors;
	}

	public Hashtable<Integer, Jugador> getJugadors() {
		return equip;
	}

	@Override
	public String toString() {
		return "Equip " + getNom() + " te " + getPuntsLliga() + " punts.";
	}
}
