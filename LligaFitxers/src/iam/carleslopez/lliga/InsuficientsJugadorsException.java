package iam.carleslopez.lliga;

@SuppressWarnings("serial")
public class InsuficientsJugadorsException extends Exception {
	
	public InsuficientsJugadorsException(String message) {		
		super(message);
	}
}
